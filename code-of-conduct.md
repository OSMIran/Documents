<div dir=rtl>

# پیمان نحوه‌ی برخورد با مشارکت‌کننده و مدیران


## تعهد ما

برای ایجاد محیط باز و خوشایند، ما به عنوان مشارکت‌کنندگان و مدیران گروه تلگرامی OSM Iran متعهد می‌شویم که شرکت کردن در پروژه‌ها و جامعه‌مان تجربه‌ای بدون آزار برای همگان، صرف نظر از سن، اندازه بدن، معلولیت، قومیت، هویت جنسی، سطح تجربه، تحصیلات، وضعیت اقتصادی، شکل ظاهری، نژاد، مذهب، تفکرات سیاسی، هویت و جهت‌گیری جنسی باشد.

## استانداردهای ما

نمونه‌ی نحوه‌ی برخورد مشارکت‌کنندگان و اعضای گروه برای ایجاد محیطی مثبت شامل موارد ذیل است:

* استفاده از زبان گرم و صمیمانه
* احترام گذاشتن به نظرات و تجربیات متفاوت
* پذیرش انتقاد سازنده با روی باز
* تمرکز کردن بر اینکه چه چیزی برای جامعه‌مان بهتر است
* ابراز همدلی با بقیه‌ی اعضای جامعه

نمونه‌ی برخورد غیرقابل‌قبول توسط شرکت‌کنندگان و اعضای گروه شامل موارد ذیل است:

* استفاده از اصطلاحات جنسیتی یا برخورد جنسیت‌زده و ناخوشایند
* اظهار‌نظر توهین‌آمیز یا ناخوشایند، و حمله‌های سیاسی یا شخصی
* آزار عمومی یا خصوصی
* منتشر کردن اطلاعات خصوصی دیگران، مثل نشانی فیزیکی یا الکترونیکی بدون کسب اجازه از آن‌ها
* هر نوع برخورد دیگری که از آن بتوان به عنوان رفتار نامناسب در محیط پروژه نام برد

# مسئولیت‌های ما

مدیران گروه موظفند که استانداردهای برخورد قابل قبول را وضوح بخشند و از آنها انتظار می‌رود که در صورت مشاهده‌ی هر گونه رفتار غیرقابل قبول از نظر این پیمان برخوردی متناسب برای تصحیح این رفتار انجام دهند.

مدیران گروه حق دارند و مسئولند که پیام‌های ارسالی در گروه، کامنت‌ها، کامیت‌ها، کد، ویرایش‌های ویکی، مشکلات و هر نوع مشارکتی که با پیمان نحوه‌ی برخورد با مشارکت‌کننده هم‌خوانی نداشته باشند را حذف، ویرایش یا رد کنند، یا به صورت موقت یا دائمی مشارکت‌کننده‌ای که رفتارهای غیرقابل قبول، تهدیدکننده، توهین‌آمیر، یا مضر انجام می دهد را تحریم کنند.

## حوزه

این پیمان هم به داخل محیط گروه و هم به فضاهای عمومی که شخص، نماینده‌ی گروه یا جامعه آن است اعمال می شود. نمونه‌ی نمایندگی یک گروه یا جامعه شامل استفاده از ایمیل رسمی گروه(user@osmiran.ir)، ارسال پست از حساب کاربری رسمی در شبکه‌های اجتماعی، یا فعالیت به عنوان نماینده‌ی برگزیده در یک رویداد آنلاین یا آفلاین می‌شود. نمایندگی یک گروه می‌تواند توسط مدیران گروه تعریف و تصریح شود.

## اجرا کردن

هرگونه برخورد سوءاستفاده‌گرایانه، آزاردهنده، یا به هرشکل غیرقابل‌قبول می‌بایست به تیم پروژه اطلاع داده شود. تمامی شکایات بررسی و پیگیری خواهند شد و پاسخ مناسبی با توجه به شرایط داده خواهد شد. تیم پروژه موظف است محرمانگی اطلاعات گزارش دهنده‌ی رخداد را حفظ کند.

<b> مدیران باید بطور کامل و شفاف وضعیت شغلی خود را بیان کنند. این برای این است که مدیر تضاد منافعی هم با پروژه‌ی OpenStreetMap و هم با تصمیماتی که در گروه مدیران OSM Iran گرفته میشود نداشته باشد.
اگر فردی بعد از عضویت در گروه مدیران، مشغول به کاری شود که آن شغل تضاد منافعی با OpenStreetMap یا OSM Iran داشته باشد، باید به سایر مدیران اطلاع دهد. در صورت عدم اطلاع‌رسانی، مدیر خاطی با مجازات‌های موقت یا دائمی که توسط مدیران اصلی گروه و یا سایر مدیران تعیین می شود مواجه خواهد شد.  </b> 

توضیحات در مورد سیاست‌های خاص دیگر هم می تواند به طور جداگانه اضافه شود.

 مدیرانی که از این پیمان پیروی یا آن را اجرا نکنند با مجازات‌های موقت یا دائمی که توسط مدیران اصلی گروه و یا سایر مدیران تعیین می شود مواجه خواهند شد.

## ارجاع

این پیمان نحوه‌ی برخورد از [پیمان مشارکت](https://www.contributor-covenant.org/)، نسخه ۱.۴، که در آدرس
https://www.contributor-covenant.org/version/1/4/code-of-conduct.html در دسترس است
برداشته و با مقداری ویرایش منتشر شده است.
